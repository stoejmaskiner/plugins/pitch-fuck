use nih_plug::prelude::*;
use nih_plug_vizia::ViziaState;
use std::sync::Arc;

// This is a shortened version of the gain example with most comments removed, check out
// https://github.com/robbert-vdh/nih-plug/blob/master/plugins/examples/gain/src/lib.rs to get
// started

mod editor;

struct PitchFuck {
    params: Arc<PitchFuckParams>,
}

#[derive(Params)]
struct PitchFuckParams {
    /// The editor state, saved together with the parameter state so the custom scaling can be
    /// restored.
    #[persist = "editor-state"]
    editor_state: Arc<ViziaState>,

    #[id = "bypass"]
    pub bypass: BoolParam,

    #[id = "gain"]
    pub gain: FloatParam,

    #[id = "global_mix"]
    pub global_mix: FloatParam,

    // TODO: rename to pitch_1 after porting old code
    #[id = "pitch_1"]
    pub div: FloatParam,

    #[id = "bias_1"]
    pub bias: FloatParam,

    #[id = "chaos_1"]
    pub chaos: FloatParam,

    #[id = "perturb_1"]
    pub perturb: FloatParam,

    #[id = "sensing_lp_1"]
    pub sensing_lp: FloatParam,

    #[id = "mix_1"]
    pub mix_1: FloatParam,

    #[id = "pitch_2"]
    pub pitch_2: FloatParam,

    #[id = "bias_2"]
    pub bias_2: FloatParam,

    #[id = "chaos_2"]
    pub chaos_2: FloatParam,

    #[id = "perturb_2"]
    pub perturb_2: FloatParam,

    #[id = "sensing_lp_2"]
    pub sensing_lp_2: FloatParam,

    #[id = "mix_2"]
    pub mix_2: FloatParam,
}

impl Default for PitchFuck {
    fn default() -> Self {
        Self {
            params: Arc::new(PitchFuckParams::default()),
        }
    }
}

impl Default for PitchFuckParams {
    fn default() -> Self {
        use FloatRange::Linear;

        Self {
            editor_state: editor::default_state(),
            bypass: BoolParam::new("(unused)", false),

            // TODO: this should have skewed range, see commented out block below
            gain: FloatParam::new("(unused)", 0.0, Linear { min: 0.0, max: 1.0 }),

            global_mix: FloatParam::new("(unused)", 0.0, Linear { min: 0.0, max: 1.0 }),

            // TODO: Div should really be an int parameter, but as long as I'm just porting
            //       old code I will leave it unchanged.
            div: FloatParam::new(
                "Div",
                0.0,
                Linear {
                    min: 1.0,
                    max: 11.5,
                },
            ),

            bias: FloatParam::new(
                "Bias",
                0.0,
                Linear {
                    min: -1.0,
                    max: 1.0,
                },
            ),

            chaos: FloatParam::new("Chaos", 0.0, Linear { min: 0.0, max: 1.0 }),

            // TODO: should really follow a dB scale
            perturb: FloatParam::new(
                "Perturb",
                0.0,
                Linear {
                    min: 0.0,
                    max: 1.0 / 40.0,
                },
            ),

            // TODO: should really follow a Hz scale
            sensing_lp: FloatParam::new(
                "Sensing LP",
                0.0,
                Linear {
                    min: 0.0,
                    max: 20_000.0,
                },
            ),

            mix_1: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            pitch_2: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            bias_2: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            chaos_2: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            perturb_2: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            sensing_lp_2: FloatParam::new(
                "(unused)",
                0.0,
                FloatRange::Linear { min: 0.0, max: 1.0 },
            ),
            mix_2: FloatParam::new("(unused)", 0.0, FloatRange::Linear { min: 0.0, max: 1.0 }),
            /*
            // This gain is stored as linear gain. NIH-plug comes with useful conversion functions
            // to treat these kinds of parameters as if we were dealing with decibels. Storing this
            // as decibels is easier to work with, but requires a conversion for every sample.
            gain: FloatParam::new(
                "Gain",
                util::db_to_gain(0.0),
                FloatRange::Skewed {
                    min: util::db_to_gain(-30.0),
                    max: util::db_to_gain(30.0),
                    // This makes the range appear as if it was linear when displaying the values as
                    // decibels
                    factor: FloatRange::gain_skew_factor(-30.0, 30.0),
                },
            )
            // Because the gain parameter is stored as linear gain instead of storing the value as
            // decibels, we need logarithmic smoothing
            .with_smoother(SmoothingStyle::Logarithmic(50.0))
            .with_unit(" dB")
            // There are many predefined formatters we can use here. If the gain was stored as
            // decibels instead of as a linear gain value, we could have also used the
            // `.with_step_size(0.1)` function to get internal rounding.
            .with_value_to_string(formatters::v2s_f32_gain_to_db(2))
            .with_string_to_value(formatters::s2v_f32_gain_to_db()),
             */
        }
    }
}

impl Plugin for PitchFuck {
    const NAME: &'static str = "STOEJ Pitch Fuck";
    const VENDOR: &'static str = "Stoejmaskiner";
    const URL: &'static str = env!("CARGO_PKG_HOMEPAGE");
    const EMAIL: &'static str = "panierilorenzo@gmail.com";

    const VERSION: &'static str = env!("CARGO_PKG_VERSION");

    // The first audio IO layout is used as the default. The other layouts may be selected either
    // explicitly or automatically by the host or the user depending on the plugin API/backend.
    const AUDIO_IO_LAYOUTS: &'static [AudioIOLayout] = &[AudioIOLayout {
        main_input_channels: NonZeroU32::new(2),
        main_output_channels: NonZeroU32::new(2),

        aux_input_ports: &[],
        aux_output_ports: &[],

        // Individual ports and the layout as a whole can be named here. By default these names
        // are generated as needed. This layout will be called 'Stereo', while a layout with
        // only one input and output channel would be called 'Mono'.
        names: PortNames::const_default(),
    }];

    const MIDI_INPUT: MidiConfig = MidiConfig::None;
    const MIDI_OUTPUT: MidiConfig = MidiConfig::None;

    const SAMPLE_ACCURATE_AUTOMATION: bool = true;

    // If the plugin can send or receive SysEx messages, it can define a type to wrap around those
    // messages here. The type implements the `SysExMessage` trait, which allows conversion to and
    // from plain byte buffers.
    type SysExMessage = ();
    // More advanced plugins can use this to run expensive background tasks. See the field's
    // documentation for more information. `()` means that the plugin does not have any background
    // tasks.
    type BackgroundTask = ();

    fn params(&self) -> Arc<dyn Params> {
        self.params.clone()
    }

    fn editor(&mut self, _async_executor: AsyncExecutor<Self>) -> Option<Box<dyn Editor>> {
        editor::create(self.params.clone(), self.params.editor_state.clone())
    }

    fn initialize(
        &mut self,
        _audio_io_layout: &AudioIOLayout,
        _buffer_config: &BufferConfig,
        _context: &mut impl InitContext<Self>,
    ) -> bool {
        // Resize buffers and perform other potentially expensive initialization operations here.
        // The `reset()` function is always called right after this function. You can remove this
        // function if you do not need it.
        true
    }

    fn reset(&mut self) {
        // Reset buffers and envelopes here. This can be called from the audio thread and may not
        // allocate. You can remove this function if you do not need it.
    }

    fn process(
        &mut self,
        buffer: &mut Buffer,
        _aux: &mut AuxiliaryBuffers,
        _context: &mut impl ProcessContext<Self>,
    ) -> ProcessStatus {
        for channel_samples in buffer.iter_samples() {
            // Smoothing is optionally built into the parameters themselves
            let gain = self.params.gain.smoothed.next();

            for sample in channel_samples {
                *sample *= gain;
            }
        }

        ProcessStatus::Normal
    }
}

impl ClapPlugin for PitchFuck {
    const CLAP_ID: &'static str = "com.stoejmaskiner.pitch-fuck";
    const CLAP_DESCRIPTION: Option<&'static str> = Some("Really glitchy pitch shifting");
    const CLAP_MANUAL_URL: Option<&'static str> = Some(Self::URL);
    const CLAP_SUPPORT_URL: Option<&'static str> = None;

    // Don't forget to change these features
    const CLAP_FEATURES: &'static [ClapFeature] = &[ClapFeature::AudioEffect, ClapFeature::Stereo];
}

impl Vst3Plugin for PitchFuck {
    const VST3_CLASS_ID: [u8; 16] = *b"Exactly16Chars!!";

    // And also don't forget to change these categories
    const VST3_SUBCATEGORIES: &'static [Vst3SubCategory] =
        &[Vst3SubCategory::Fx, Vst3SubCategory::Dynamics];
}

nih_export_clap!(PitchFuck);
nih_export_vst3!(PitchFuck);
